
package net.mcreator.moneyheart.itemgroup;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;

import net.mcreator.moneyheart.item.TabIconItem;
import net.mcreator.moneyheart.MoneyHeartModElements;

@MoneyHeartModElements.ModElement.Tag
public class MoneyModItemGroup extends MoneyHeartModElements.ModElement {
	public MoneyModItemGroup(MoneyHeartModElements instance) {
		super(instance, 37);
	}

	@Override
	public void initElements() {
		tab = new ItemGroup("tabmoney_mod") {
			@OnlyIn(Dist.CLIENT)
			@Override
			public ItemStack createIcon() {
				return new ItemStack(TabIconItem.block);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}

	public static ItemGroup tab;
}
