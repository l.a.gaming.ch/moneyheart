
package net.mcreator.moneyheart.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.world.World;
import net.minecraft.util.Hand;
import net.minecraft.util.ActionResult;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;
import net.minecraft.entity.player.PlayerEntity;

import net.mcreator.moneyheart.procedures.BusterSwordYoukuritukusitatokiProcedure;
import net.mcreator.moneyheart.itemgroup.MoneyModItemGroup;
import net.mcreator.moneyheart.MoneyHeartModElements;

import java.util.Collections;

@MoneyHeartModElements.ModElement.Tag
public class BusterSwordItem extends MoneyHeartModElements.ModElement {
	@ObjectHolder("money_heart:buster_sword")
	public static final Item block = null;

	public BusterSwordItem(MoneyHeartModElements instance) {
		super(instance, 53);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new SwordItem(new IItemTier() {
			public int getMaxUses() {
				return 1000;
			}

			public float getEfficiency() {
				return 4f;
			}

			public float getAttackDamage() {
				return 18f;
			}

			public int getHarvestLevel() {
				return 1;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.EMPTY;
			}
		}, 3, -2f, new Item.Properties().group(MoneyModItemGroup.tab)) {
			@Override
			public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity entity, Hand hand) {
				ActionResult<ItemStack> retval = super.onItemRightClick(world, entity, hand);
				ItemStack itemstack = retval.getResult();
				double x = entity.getPosX();
				double y = entity.getPosY();
				double z = entity.getPosZ();

				BusterSwordYoukuritukusitatokiProcedure.executeProcedure(Collections.emptyMap());
				return retval;
			}
		}.setRegistryName("buster_sword"));
	}
}
