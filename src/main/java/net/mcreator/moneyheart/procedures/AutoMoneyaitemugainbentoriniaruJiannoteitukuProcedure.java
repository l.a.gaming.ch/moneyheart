package net.mcreator.moneyheart.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.moneyheart.MoneyHeartModVariables;
import net.mcreator.moneyheart.MoneyHeartMod;

import java.util.Map;

public class AutoMoneyaitemugainbentoriniaruJiannoteitukuProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency entity for procedure AutoMoneyaitemugainbentoriniaruJiannoteituku!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double NumCnt = 0;
		if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new MoneyHeartModVariables.PlayerVariables())).autoplus <= 0) {
			{
				double _setval = 1;
				entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.autoplus = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
		}
		if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new MoneyHeartModVariables.PlayerVariables())).autoplus >= 20) {
			if (entity.getPersistentData().getDouble("health") >= 12) {
				{
					double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 100);
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
				{
					double _setval = 1;
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.autoplus = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
			} else if (entity.getPersistentData().getDouble("health") >= 6) {
				{
					double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 10);
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
				{
					double _setval = 1;
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.autoplus = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
			} else {
				{
					double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 1);
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
				{
					double _setval = 1;
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.autoplus = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
			}
		} else {
			{
				double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new MoneyHeartModVariables.PlayerVariables())).autoplus + 1);
				entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.autoplus = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
		}
	}
}
