package net.mcreator.moneyheart.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.moneyheart.MoneyHeartMod;

import java.util.Map;

public class Slot1viewProcedure {

	public static boolean executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency entity for procedure Slot1view!");
			return false;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity.getPersistentData().getDouble("health") >= 10) {
			return true;
		} else {
			return false;
		}
	}
}
