package net.mcreator.moneyheart.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;

import net.minecraft.world.World;
import net.minecraft.util.ResourceLocation;
import net.minecraft.tags.EntityTypeTags;
import net.minecraft.entity.Entity;

import net.mcreator.moneyheart.MoneyHeartModVariables;
import net.mcreator.moneyheart.MoneyHeartMod;

import java.util.Map;
import java.util.HashMap;

public class SetmobkillProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onEntityDeath(LivingDeathEvent event) {
			if (event != null && event.getEntity() != null) {
				Entity entity = event.getEntity();
				Entity sourceentity = event.getSource().getTrueSource();
				double i = entity.getPosX();
				double j = entity.getPosY();
				double k = entity.getPosZ();
				World world = entity.world;
				Map<String, Object> dependencies = new HashMap<>();
				dependencies.put("x", i);
				dependencies.put("y", j);
				dependencies.put("z", k);
				dependencies.put("world", world);
				dependencies.put("entity", entity);
				dependencies.put("sourceentity", sourceentity);
				dependencies.put("event", event);
				executeProcedure(dependencies);
			}
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency entity for procedure Setmobkill!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency sourceentity for procedure Setmobkill!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (EntityTypeTags.getCollection().getTagByID(new ResourceLocation("forge:mobkill_3")).contains(entity.getType())) {
			if (entity.getPersistentData().getDouble("health") >= 12) {
				{
					double _setval = ((sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 300);
					sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(sourceentity);
					});
				}
			} else if (entity.getPersistentData().getDouble("health") >= 6) {
				{
					double _setval = ((sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 30);
					sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(sourceentity);
					});
				}
			} else {
				{
					double _setval = ((sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 3);
					sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(sourceentity);
					});
				}
			}
		} else {
			if (EntityTypeTags.getCollection().getTagByID(new ResourceLocation("forge:mobkill_5")).contains(entity.getType())) {
				if (entity.getPersistentData().getDouble("health") >= 12) {
					{
						double _setval = ((sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
								.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 500);
						sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
							capability.money = _setval;
							capability.syncPlayerVariables(sourceentity);
						});
					}
				} else if (entity.getPersistentData().getDouble("health") >= 6) {
					{
						double _setval = ((sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
								.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 50);
						sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
							capability.money = _setval;
							capability.syncPlayerVariables(sourceentity);
						});
					}
				} else {
					{
						double _setval = ((sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
								.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 5);
						sourceentity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
							capability.money = _setval;
							capability.syncPlayerVariables(sourceentity);
						});
					}
				}
			}
		}
	}
}
