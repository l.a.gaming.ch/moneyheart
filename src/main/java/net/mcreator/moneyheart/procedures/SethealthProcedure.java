package net.mcreator.moneyheart.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.util.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.moneyheart.MoneyHeartModVariables;
import net.mcreator.moneyheart.MoneyHeartMod;

import java.util.Map;
import java.util.HashMap;

public class SethealthProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onPlayerTick(TickEvent.PlayerTickEvent event) {
			if (event.phase == TickEvent.Phase.END) {
				Entity entity = event.player;
				World world = entity.world;
				double i = entity.getPosX();
				double j = entity.getPosY();
				double k = entity.getPosZ();
				Map<String, Object> dependencies = new HashMap<>();
				dependencies.put("x", i);
				dependencies.put("y", j);
				dependencies.put("z", k);
				dependencies.put("world", world);
				dependencies.put("entity", entity);
				dependencies.put("event", event);
				executeProcedure(dependencies);
			}
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency entity for procedure Sethealth!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new MoneyHeartModVariables.PlayerVariables())).money >= 10
				&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 2) {
			entity.getPersistentData().putDouble("health", 4);
		} else {
			if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
					.orElse(new MoneyHeartModVariables.PlayerVariables())).money >= 100
					&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 4) {
				entity.getPersistentData().putDouble("health", 6);
			} else {
				if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new MoneyHeartModVariables.PlayerVariables())).money >= 1000
						&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 6) {
					entity.getPersistentData().putDouble("health", 8);
				} else {
					if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money >= 10000
							&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 8) {
						entity.getPersistentData().putDouble("health", 10);
					} else {
						if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
								.orElse(new MoneyHeartModVariables.PlayerVariables())).money >= 100000
								&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 10) {
							entity.getPersistentData().putDouble("health", 12);
						} else {
							if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
									.orElse(new MoneyHeartModVariables.PlayerVariables())).money >= 1000000
									&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 12) {
								entity.getPersistentData().putDouble("health", 14);
							} else {
								if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
										.orElse(new MoneyHeartModVariables.PlayerVariables())).money >= 10000000
										&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 14) {
									entity.getPersistentData().putDouble("health", 16);
								} else {
									if ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
											.orElse(new MoneyHeartModVariables.PlayerVariables())).money >= 100000000
											&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 16) {
										entity.getPersistentData().putDouble("health", 18);
										if ((((entity instanceof ServerPlayerEntity) && (entity.world instanceof ServerWorld))
												? ((ServerPlayerEntity) entity).getAdvancements()
														.getProgress(((MinecraftServer) ((ServerPlayerEntity) entity).server).getAdvancementManager()
																.getAdvancement(new ResourceLocation("minecraft:end/kill_dragon")))
														.isDone()
												: false) && ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getMaxHealth() : -1) <= 18) {
											entity.getPersistentData().putDouble("health", 20);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
