package net.mcreator.moneyheart.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.moneyheart.MoneyHeartMod;

import java.util.Map;

public class Slot0viewProcedure {

	public static boolean executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency entity for procedure Slot0view!");
			return false;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity.getPersistentData().getDouble("health") >= 8) {
			return true;
		} else {
			return false;
		}
	}
}
