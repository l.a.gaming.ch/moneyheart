package net.mcreator.moneyheart.procedures;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.moneyheart.MoneyHeartMod;

import java.util.Map;

public class Bill10mhalfProcedure {

	public static boolean executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency entity for procedure Bill10mhalf!");
			return false;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1) >= 15) {
			return true;
		}
		return false;
	}
}
