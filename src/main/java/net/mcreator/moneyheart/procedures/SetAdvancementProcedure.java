package net.mcreator.moneyheart.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.player.AdvancementEvent;

import net.minecraft.world.World;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.advancements.Advancement;

import net.mcreator.moneyheart.MoneyHeartModVariables;
import net.mcreator.moneyheart.MoneyHeartMod;

import java.util.Map;
import java.util.HashMap;

public class SetAdvancementProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onAdvancement(AdvancementEvent event) {
			PlayerEntity entity = event.getPlayer();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			Advancement advancement = event.getAdvancement();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("advancement", advancement);
			dependencies.put("event", event);
			executeProcedure(dependencies);
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency entity for procedure SetAdvancement!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity.getPersistentData().getDouble("health") >= 12) {
			{
				double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 500);
				entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.money = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
		} else if (entity.getPersistentData().getDouble("health") >= 6) {
			{
				double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 50);
				entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.money = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
		} else {
			{
				double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 5);
				entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.money = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
		}
	}
}
