package net.mcreator.moneyheart.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.world.BlockEvent;

import net.minecraft.world.IWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.entity.Entity;
import net.minecraft.block.Blocks;

import net.mcreator.moneyheart.MoneyHeartModVariables;
import net.mcreator.moneyheart.MoneyHeartMod;

import java.util.Map;
import java.util.HashMap;

public class SetblockbreakProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onBlockBreak(BlockEvent.BreakEvent event) {
			Entity entity = event.getPlayer();
			IWorld world = event.getWorld();
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("xpAmount", event.getExpToDrop());
			dependencies.put("x", event.getPos().getX());
			dependencies.put("y", event.getPos().getY());
			dependencies.put("z", event.getPos().getZ());
			dependencies.put("px", entity.getPosX());
			dependencies.put("py", entity.getPosY());
			dependencies.put("pz", entity.getPosZ());
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("blockstate", event.getState());
			dependencies.put("event", event);
			executeProcedure(dependencies);
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency world for procedure Setblockbreak!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency x for procedure Setblockbreak!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency y for procedure Setblockbreak!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency z for procedure Setblockbreak!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MoneyHeartMod.LOGGER.warn("Failed to load dependency entity for procedure Setblockbreak!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		if ((world.getBlockState(new BlockPos(x, y, z))).getBlock() == Blocks.STONE) {
			if (entity.getPersistentData().getDouble("health") >= 12) {
				{
					double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 100);
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
			} else if (entity.getPersistentData().getDouble("health") >= 6) {
				{
					double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 10);
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
			} else {
				{
					double _setval = ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new MoneyHeartModVariables.PlayerVariables())).money + 1);
					entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
						capability.money = _setval;
						capability.syncPlayerVariables(entity);
					});
				}
			}
		}
	}
}
