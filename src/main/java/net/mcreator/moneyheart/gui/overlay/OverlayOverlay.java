
package net.mcreator.moneyheart.gui.overlay;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.World;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.client.Minecraft;

import net.mcreator.moneyheart.procedures.Coin1halfProcedure;
import net.mcreator.moneyheart.procedures.Coin1Procedure;
import net.mcreator.moneyheart.procedures.Coin10halfProcedure;
import net.mcreator.moneyheart.procedures.Coin10frameviewProcedure;
import net.mcreator.moneyheart.procedures.Coin10Procedure;
import net.mcreator.moneyheart.procedures.Coin100halfProcedure;
import net.mcreator.moneyheart.procedures.Coin100frameviewProcedure;
import net.mcreator.moneyheart.procedures.Coin100Procedure;
import net.mcreator.moneyheart.procedures.Bill1tProcedure;
import net.mcreator.moneyheart.procedures.Bill1mhalfProcedure;
import net.mcreator.moneyheart.procedures.Bill1mframeviewProcedure;
import net.mcreator.moneyheart.procedures.Bill1mProcedure;
import net.mcreator.moneyheart.procedures.Bill1khalfProcedure;
import net.mcreator.moneyheart.procedures.Bill1kframeviewProcedure;
import net.mcreator.moneyheart.procedures.Bill1kProcedure;
import net.mcreator.moneyheart.procedures.Bill10mhalfProcedure;
import net.mcreator.moneyheart.procedures.Bill10mframeviewProcedure;
import net.mcreator.moneyheart.procedures.Bill10mProcedure;
import net.mcreator.moneyheart.procedures.Bill10khalfProcedure;
import net.mcreator.moneyheart.procedures.Bill10kframeviewProcedure;
import net.mcreator.moneyheart.procedures.Bill10kProcedure;
import net.mcreator.moneyheart.procedures.Bill100mhalfProcedure;
import net.mcreator.moneyheart.procedures.Bill100mframeviewProcedure;
import net.mcreator.moneyheart.procedures.Bill100mProcedure;
import net.mcreator.moneyheart.procedures.Bill100khalfProcedure;
import net.mcreator.moneyheart.procedures.Bill100kframeviewProcedure;
import net.mcreator.moneyheart.procedures.Bill100kProcedure;
import net.mcreator.moneyheart.MoneyHeartModVariables;

import java.util.stream.Stream;
import java.util.Map;
import java.util.HashMap;
import java.util.AbstractMap;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.platform.GlStateManager;

@Mod.EventBusSubscriber
public class OverlayOverlay {
	@OnlyIn(Dist.CLIENT)
	@SubscribeEvent(priority = EventPriority.NORMAL)
	public static void eventHandler(RenderGameOverlayEvent.Post event) {
		if (event.getType() == RenderGameOverlayEvent.ElementType.HELMET) {
			int w = event.getWindow().getScaledWidth();
			int h = event.getWindow().getScaledHeight();
			int posX = w / 2;
			int posY = h / 2;
			World _world = null;
			double _x = 0;
			double _y = 0;
			double _z = 0;
			PlayerEntity entity = Minecraft.getInstance().player;
			if (entity != null) {
				_world = entity.world;
				_x = entity.getPosX();
				_y = entity.getPosY();
				_z = entity.getPosZ();
			}
			World world = _world;
			double x = _x;
			double y = _y;
			double z = _z;
			RenderSystem.disableDepthTest();
			RenderSystem.depthMask(false);
			RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA,
					GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
			RenderSystem.disableAlphaTest();
			if (true) {
				Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/coin_frame.png"));
				Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -94, posY + 94, 0, 0, 20, 10, 20, 10);

				if (Coin1halfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -94, posY + 94, 0, 0, 20, 10, 20, 10);
				}
				if (Coin1Procedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -94, posY + 94, 0, 0, 20, 10, 20, 10);
				}
				if (Coin10frameviewProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/coin_frame.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -82, posY + 94, 0, 0, 20, 10, 20, 10);
				}
				if (Coin10halfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/10_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -82, posY + 94, 0, 0, 20, 10, 20, 10);
				}
				if (Coin10Procedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/10.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -82, posY + 94, 0, 0, 20, 10, 20, 10);
				}
				if (Coin100frameviewProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/coin_frame.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -70, posY + 94, 0, 0, 20, 10, 20, 10);
				}
				if (Coin100halfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/100_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -70, posY + 94, 0, 0, 20, 10, 20, 10);
				}
				if (Coin100Procedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/100.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -70, posY + 94, 0, 0, 20, 10, 20, 10);
				}
				if (Bill1kframeviewProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1k_frame.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -53, posY + 89, 0, 0, 16, 16, 16, 16);
				}
				if (Bill1khalfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1k_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -53, posY + 89, 0, 0, 16, 16, 16, 16);
				}
				if (Bill1kProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1k.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -53, posY + 89, 0, 0, 16, 16, 16, 16);
				}
				if (Bill10kframeviewProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/10k_frame.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -40, posY + 89, 0, 0, 16, 16, 16, 16);
				}
				if (Bill10khalfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/10k_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -40, posY + 89, 0, 0, 16, 16, 16, 16);
				}
				if (Bill10kProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/10k.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -40, posY + 89, 0, 0, 16, 16, 16, 16);
				}
				if (Bill100kframeviewProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/100k_frame.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -24, posY + 88, 0, 0, 16, 16, 16, 16);
				}
				if (Bill100khalfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/100k_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -24, posY + 88, 0, 0, 16, 16, 16, 16);
				}
				if (Bill100kProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/100k.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -24, posY + 88, 0, 0, 16, 16, 16, 16);
				}
				if (Bill1mframeviewProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1m_frame.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -72, posY + 64, 0, 0, 32, 32, 32, 32);
				}
				if (Bill1mhalfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1m_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -73, posY + 64, 0, 0, 32, 32, 32, 32);
				}
				if (Bill1mProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1m.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -72, posY + 64, 0, 0, 32, 32, 32, 32);
				}
				if (Bill10mframeviewProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/10m_frame.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -104, posY + 62, 0, 0, 32, 32, 32, 32);
				}
				if (Bill10mhalfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/10m_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -104, posY + 62, 0, 0, 32, 32, 32, 32);
				}
				if (Bill10mProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/10m.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -104, posY + 62, 0, 0, 32, 32, 32, 32);
				}
				if (Bill100mframeviewProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/100m_frame.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -43, posY + 60, 0, 0, 32, 32, 32, 32);
				}
				if (Bill100mhalfProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/100m_half.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -43, posY + 59, 0, 0, 32, 32, 32, 32);
				}
				if (Bill100mProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/100m.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -43, posY + 59, 0, 0, 32, 32, 32, 32);
				}
				if (Bill1tProcedure.executeProcedure(Stream.of(new AbstractMap.SimpleEntry<>("entity", entity)).collect(HashMap::new,
						(_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("money_heart:textures/1t.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -104, posY + 30, 0, 0, 96, 32, 96, 32);
				}
				Minecraft.getInstance().fontRenderer.drawString(event.getMatrixStack(),
						"" + (int) ((entity.getCapability(MoneyHeartModVariables.PLAYER_VARIABLES_CAPABILITY, null)
								.orElse(new MoneyHeartModVariables.PlayerVariables())).money) + "",
						posX + -201, posY + -103, -1);
			}
			RenderSystem.depthMask(true);
			RenderSystem.enableDepthTest();
			RenderSystem.enableAlphaTest();
			RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}
